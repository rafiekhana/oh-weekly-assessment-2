// write function here
const oddEvenArray = (array) => {
  for(let i = 0; i < array.length; i++) {
    if(array[i] % 2 === 0) {
      console.log(`${array[i]} is even number`)
    } else {
      console.log(`${array[i]} is odd number`)
    }
  }
  return array;
};

// input test
const input1 = [1, 4, 6, 3, 10, 7];

oddEvenArray(input1);
