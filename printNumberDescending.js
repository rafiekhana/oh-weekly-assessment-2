const printNumberDescending = (number) => {
  for(let i = number; i >= 0; i--) {
    console.log(i);
  }
  return number;
};

const input1 = 5;
const input2 = 10;

printNumberDescending(input1);
printNumberDescending(input2);
