// write function here
const sumArray = (number) => {
  let sum = 0;
  let addition = 0
  for(let i = 0; i < number.length; i++) {
    sum = number[i] + addition;
    addition = sum;
  }
  return sum;
};

// input test
const input1 = [1, 2, 5, 8, 9, 10];
const input2 = [1, 2, 3, 4, 5];

const result = sumArray(input1); // output: 35
console.log(result);

const output = sumArray(input2); // output: 15
console.log(output);