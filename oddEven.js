// write function here
const oddEven = (number) => {
  for(let i = number; i <= number; i++) {
    if(number % 2 === 0) {
      console.log(`${number} is even number`)
    } else {
      console.log(`${number} is odd number`)
    }
  }
  return number;
};

// input test
const input1 = 3;
const input2 = 10;

oddEven(input1); // output: 3 is odd number
oddEven(input2);