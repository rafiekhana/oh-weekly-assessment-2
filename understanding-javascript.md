# Task 1: Understanding Javascript

1. What is the difference between **Javascript** and **HTML**?

=> **JavaScript** is a scripting language that can be used to control or process data and other things. While **HTML** is a markup language that can be used to organize content on websites.

2. What is the differenece between `var`, `let`, and `const`?

=> `var` variable is a globally scoped variable and can be updated or re-declared within its scope; `let` variable is a block scoped variable and can be updated but not re-declared; while `const` variable is a block scoped variable and can neither be updated nor re-declared.

3. What **data types** in javascript do you know?

=> In javascript there are several types of **data types** such as number, string, boolean, undefined, null, and object.

4. What do you know about `function`?

=> `Function` in javascript is a block of code designed to perform a particular or specific task that is made to be called back using the return syntax.

5. What do you know about **hoisting**?

=> **Hoisting** in javascript is a process of moving declared variables to global.
